#ifndef BUY_HPP
#define BUY_HPP
#include "Date.hpp"
#include <time.h>
#include <stdexcept>
#include "good.hpp"

class Buy {

private:
  unsigned int marketId;
  unsigned int goodId;
  int price;
  int count;
  Date date;

public:
  Buy(unsigned int ID, int P, int cnt, Date D);
  unsigned int getmarketId();
  unsigned int getGoodId();
  int Getprice();
  int getCount();
  Date getDate();
};



#endif // BUY_HPP
