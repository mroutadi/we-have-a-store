
#ifndef STORE_HPP
#define STORE_HPP
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include "sold.hpp"
#include "buy.hpp"

class Store {
private:
  std::vector <Sold> Solds;
  std::vector <Buy> Boughts;
  std::vector <good> Goods;
  std::vector <int> gdIds;    //vector for save goods ids
  std::vector <int> byIds;    //vector for save buys ids
  std::vector <int> slIds;    //vector for save sells ids
  
public:
  Store();
  void addSolds(Sold);
  std::vector <int> getByIds();
  std::vector <int> getGdIds();
  std::vector <int> getSlIds();
  std::vector<Sold> getSolds();
  void addBoughts(Buy);
  std::vector<Buy> getBoughts();
  void addGood(good);
  std::vector<good> getGood();
  void deleteGood(std::string);
};

#endif // STORE_HPP
