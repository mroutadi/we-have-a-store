#ifndef DATE_HPP
#define DATE_HPP
#include <string>

class Date {
private:
  unsigned int Year;
  unsigned int Month;
  unsigned int Day;
  unsigned int Hour;
  unsigned int Minute;
public:
  Date();
  Date(unsigned int,unsigned int,unsigned int,unsigned int,unsigned int, bool); //bool for check is by computer
  std::string getTime();
  std::string getDate();
  std::string getFDate();
  unsigned int getYear();
  unsigned int getMonth();
  unsigned int getDay();
  unsigned int getHour();
  unsigned int getMinute();

};

#endif // DATE_HPP
