
#ifndef GOOD_HPP
#define GOOD_HPP
#include <iostream>
#include <string>
#include <iomanip>
#include <time.h>
#include <vector>
#include <stdexcept>
#include "Date.hpp"

class good {
private:
  unsigned int id;
  std::string name;
  std::string KeepType;
  std::string Brand;
  Date publish;
  Date expiration;
  int goodCount;

public:
  good (std::string N, int ID, std::string KT, std::string B, Date P, Date E);
  void changeInfo(std::string);
  std::string getName();
  int getId();
  int getCount();
  std::string getKeepType();
  std::string getBrand();
  Date getPubDate();
  Date getExpDate();
  void addCount(int);
  void minCount(int);
};

#endif // GOOD_HPP