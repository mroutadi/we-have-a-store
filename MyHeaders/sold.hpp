#ifndef SOLD_HPP
#define SOLD_HPP
#include "Date.hpp"
#include <time.h>
#include <random>

class Sold {

private:
  unsigned int cusId;
  unsigned int goodId;
  int price;
  int count;
  Date date;
  int Off;

public:
  Sold(unsigned int I, int P, int cnt, Date D, int O);
  unsigned int getCusId();
  unsigned int getGoodId();
  int Getprice();
  int getCount();
  Date getDate();
  int getOff();

};


#endif // SOLD_HPP
