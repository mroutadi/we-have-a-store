#include "MyHeaders/sold.hpp"

Sold::Sold(unsigned int I, int P, int cnt, Date D, int O) :
goodId(I), price(P), count(cnt), date(D), Off(O)
{
  srand(time(NULL));
  this->cusId = rand()%9000 + 999;
}

unsigned int Sold::getCusId() {
  return this->cusId;
}

unsigned int Sold::getGoodId() {
  return this->goodId;
}

int Sold::Getprice() {
  return this->price;
}

int Sold::getCount() {
  return this->count;
}

Date Sold::getDate() {
  return this->date;
}

int Sold::getOff() {
  return this->Off;
}
