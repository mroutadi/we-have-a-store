#include <iostream>
#include <string>
#include <vector>
#include <iomanip>
#include <fstream>
#include <time.h>
#include "MyHeaders/Store.hpp"

void InputandDo(Store*);
void addProduct(Store*);
void deleteProduct(Store*);
void editProduct(Store*);
void showProduct(Store);
void findProduct(Store);
void buyProduct(Store*);
void sellProduct(Store*);
void bill(Store);


using namespace std;

int main()
{
    cout << "Welcome !" << endl;
    Store FBO;              //this is our Market
    //Store Reloading
    fstream gd;            //Goods reload
    gd.open("product.dat", ios::binary | ios::in | ios::out | ios::trunc ); //Open files to reload datas into Store
    if (!gd.is_open())
    {
        cerr << "Your Good's hisory didn't Overload!(If it's a new store or you didn't need that, ignore it!)" <<endl;
    }
    else
    {
        gd.seekg(0);
        int noGoods = 0;
        gd.read((char *)(&noGoods), sizeof(int));
        vector <good> lastGoods;
        gd.read((char *)(&lastGoods), noGoods * sizeof(good));
    }

    fstream by;            //Buys reload
    by.open("Buy.dat", ios::binary | ios::in | ios::out | ios::trunc ); //Open files to reload datas into Store
    if (!by.is_open())
    {
        cerr << "Your Buy's hisory didn't Overload!(If it's a new store or you didn't need that, ignore it!)" <<endl;
    }
    else
    {
        by.seekg(0);
        int noBuys = 0;
        by.read((char *)(&noBuys), sizeof(int));
        vector <Buy> lastBuy;
        by.read((char *)(&lastBuy), noBuys * sizeof(Buy));
    }
    
    fstream sl;            //Sells reload
    sl.open("Sold.dat", ios::binary | ios::in | ios::out | ios::trunc ); //Open files to reload datas into Store
    if (!sl.is_open())
    {
        cerr << "Your Sell's hisory didn't Overload!(If it's a new store or you didn't need that, ignore it!)" <<endl;
    }
    else
    {
        sl.seekg(0);
        int noSells = 0;
        sl.read((char *)(&noSells), sizeof(int));
        vector <Sold> lastSell;
        sl.read((char *)(&lastSell), noSells * sizeof(Sold));
    }
    cout << "Use these commands, their abbr or their numbers !"<<endl;
    cout << right << setw(8) << "Num  "<<left << setfill('-') << setw(40) << "Command   " << setw(12) << setfill(' ') << " Abbreviation" << endl 
    << right << setw(8) << "1  " << left << setfill('~') << setw(40) << "Add Product   " << setw(12) << setfill(' ') << " AP" << endl 
    << right << setw(8) << "2  " << left << setfill('-') << setw(40) << "Delete Existing Product   " << setw(12) << setfill(' ') << " DEP" << endl
    << right << setw(8) << "3  " << left << setfill('~') << setw(40) << "Edit Product Info   " << setw(12) << setfill(' ') << " EPI" << endl
    << right << setw(8) << "4  " << left << setfill('-') << setw(40) << "Show Product Information   " << setw(12) << setfill(' ') << " SPI" << endl
    << right << setw(8) << "5  " << left << setfill('~') << setw(40) << "Find Product   " << setw(12) << setfill(' ') << " FP" << endl
    << right << setw(8) << "6  " << left << setfill('-') << setw(40) << "Buying Product   " << setw(12) << setfill(' ') << " BP" << endl 
    << right << setw(8) << "7  " << left << setfill('~') << setw(40) << "Selling Product   " << setw(12) << setfill(' ') << " SP" << endl 
    << right << setw(8) << "8  " << left << setfill('-') << setw(40) << "Bill   " << setw(12) << setfill(' ') << " B" << endl
    << right << setw(8) << "9  " << left << setfill('-') << setw(40) << "Exit   " << setw(12) << setfill(' ') << " E" << endl;
    while (true)
    {
        InputandDo(&FBO);
        int n1 = FBO.getGood().size(),
        n2 = FBO.getBoughts().size(),
        n3 = FBO.getSolds().size();
        gd.seekg(0);
        by.seekg(0);
        sl.seekg(0);
        gd.write((const char*)(&n1),  sizeof(int));
        by.write((const char*)(&n2),  sizeof(int));
        sl.write((const char*)(&n3),  sizeof(int));
        vector <good> v1;
        vector <Buy> v2;
        vector <Sold> v3;
        for (size_t i = 0; i < FBO.getGood().size(); i++)
        {
            v1.push_back(FBO.getGood()[i]);
        }
        for (size_t i = 0; i < FBO.getBoughts().size(); i++)
        {
            v2.push_back(FBO.getBoughts()[i]);
        }
        for (size_t i = 0; i < FBO.getSolds().size(); i++)
        {
            v3.push_back(FBO.getSolds()[i]);
        }
        gd.write((const char*)(&v1),  n1 * sizeof(good));
        by.write((const char*)(&v2),  n2 * sizeof(Buy));
        sl.write((const char*)(&v3),  n3 * sizeof(Sold));
    }
    gd.close();
    by.close();
    sl.close();
    return 0;
}

void InputandDo (Store* FBO) {
    cout << "Enter Your Command : ";
    string CMNDstr;
    getline(std::cin, CMNDstr);
    vector <string> CMND;
    bool FirstChecker = false;
    string s = "";
    for (auto &i : CMNDstr)
    {
        if (i == ' ' && !FirstChecker)
        {
            continue;
        }
        else
        {
            FirstChecker = true;
        }
        if (i == ' ' && FirstChecker)
        {
            CMND.push_back(s);
            s = "";
        }
        else
        {
            if ((i < 'a' || i > 'z') && (i < 0 && i > 10))
            {
                i += 32;
            }
            s += i;
        }
    }
    CMND.push_back(s);
    if (CMND[0] == "1" || ( CMND[0] == "add" && CMND[1] == "product") || CMND[0] == "ap")
    {
        addProduct(FBO);
    }
    else if (CMND[0] == "2" || ( CMND[0] == "delete" && CMND[1] == "existing" && CMND[2] == "product") || CMND[0] == "dep")
    {
        deleteProduct(FBO);
    }
    else if (CMND[0] == "3" || ( CMND[0] == "edit" && CMND[1] == "product" ) || CMND[0] == "epi")
    {
        editProduct(FBO);
    }
    else if (CMND[0] == "4" || ( CMND[0] == "show" && CMND[1] == "product" ) || CMND[0] == "spi")
    {
        showProduct(*FBO);
    }
    else if (CMND[0] == "5" || ( CMND[0] == "find" && CMND[1] == "product" ) || CMND[0] == "fp")
    {
        findProduct(*FBO);
    }
    else if (CMND[0] == "6" || ( CMND[0] == "buying" && CMND[1] == "product" ) || CMND[0] == "bp")
    {
        buyProduct(FBO);
    }
    else if (CMND[0] == "7" || ( CMND[0] == "selling" && CMND[1] == "product" ) || CMND[0] == "sp")
    {
        sellProduct(FBO);
    }
    else if (CMND[0] == "8" || ( CMND[0] == "bill" ) || CMND[0] == "b")
    {
        bill(*FBO);
    }
    else if (CMND[0] == "9" || ( CMND[0] == "exit" ) || CMND[0] == "e")
    {
        exit(0);
    }
    else if (CMNDstr != "\n" && CMNDstr != "\r\n" && s != "" && CMNDstr.size() != 0) {
        cerr << "!!!!!     Please Enter True Command     !!!!!" <<endl;
    }
}
void addProduct (Store* store) {
    int Id;
    bool rptId = true;
    while (rptId)
    {
        srand(time(NULL));
        try
        {
            Id = rand()%900 + 99;
            for (size_t i = 0; i < store->getGdIds().size(); i++)
            {
                if (store->getGdIds()[i] == Id)
                {
                    throw std::invalid_argument("Repeated Id");
                }
            }
        }
        catch(std::invalid_argument)
        {
            continue;
        }
        rptId = false;
    }
    

    string name;
    cout << "Please Enter Good Name : ";
    std::cin >> name;
    bool isntTrue = false;
    while (!isntTrue)
    {
        if (name == "")
        {
            cerr << "!!!!!     Please Enter At Least 1 Character for Name     !!!!! : ";
            std::cin >> name;
            continue;
        }
        else
        {
            for (size_t i = 0; i < store->getGood().size(); i++)
            {
                if (name == store->getGood()[i].getName())
                {
                    cerr << "!!!!!     Please Enter unRepeated Name     !!!!! : ";
                    std::cin >> name;
                    continue;
                }
                
            }
            isntTrue = true;
            
        }
        
        
    }
    int KeepTypenum;
    string KeepType;
    cout << left << setw(4) << "1." << setw(8) << "NDist" <<endl;
    cout << left << setw(4) << "2." << setw(8) << "DNRef" <<endl;
    cout << left << setw(4) << "3." << setw(8) << "NRef " <<endl;
    cout << "Enter Keeping Type Number : ";
    std::cin >> KeepTypenum;
    bool trueKT = false;
    while (!trueKT)
    {
        if (KeepTypenum > 3 && KeepTypenum < 1)
        {
            cerr << "!!!!!     Please Enter True Option     !!!!! : ";
            std::cin >> KeepTypenum;    
        }
        else trueKT = true;
        
    }
    switch (KeepTypenum)
    {
        case 1:
        KeepType = "NDist";
        break;
    
        case 2:
        KeepType = "DNRef";
        break;
    
        case 3:
        KeepType = "NRef";
        break;
    }
    string brand;
    cout << "Please Enter Good Brand : ";
    std::cin >> brand;
    bool trueBr = false;
    while (!trueKT)
    {
        if (brand == "")
        {
            cerr << "!!!!!     Please Enter At Least 1 Character for Brand     !!!!! : ";
            std::cin >> brand;  
        }
        else trueBr = true;
        
    }
    int pYear, pMonth, pDay, pHour, pMinute;
    cout << "set now at Publish Date? (Y/n) : ";
    char Ent;
    std::cin >> Ent;
    Date PUD;
    if ('Y' == Ent)
    {
        time_t now = time(0);
        tm *Time = localtime (&now);
        pYear = Time->tm_year;
        pMonth = Time->tm_mon;
        pDay = Time->tm_mday;
        pHour = Time->tm_hour;
        pMinute = Time->tm_min;
        Date PUD(pYear, pMonth, pDay, pHour, pMinute, true);
    }
    else 
    {
        cout << "Please Enter Publish Year : ";
        std::cin >> pYear;
        while (pYear < 1300)
        {
            cerr << "!!!!!     Please Enter True Year     !!!!! : ";
            std::cin >> pYear;
        }
        cout << "Please Enter Publish Month : ";
        std::cin >> pMonth;
        while (pMonth < 1 || pMonth > 12)
        {
            cerr << "!!!!!     Please Enter True Month     !!!!! : ";
            std::cin >> pMonth;
        }
        cout << "Please Enter Publish Day : ";
        std::cin >> pDay;
        if (pMonth > 6)
        {
            while (pDay < 1 || pDay > 30)
            {
                cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                std::cin >> pDay;
            }    
        }
        else
        {
            while (pDay < 1 || pDay > 31)
            {
                cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                std::cin >> pDay;
            }    
        }
        cout << "Please Enter Publish Hour : ";
        std::cin >> pHour;
        while (pHour < 0 || pHour > 23)
        {
            cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
            std::cin >> pHour;
        }
        cout << "Please Enter Publish Minute : ";
        std::cin >> pMinute;
        while (pMinute < 0 || pMinute > 59)
        {
            cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
            std::cin >> pMinute;
        }
        Date PUD(pYear, pMonth, pDay, pHour, pMinute, false);
    }
    int eYear, eMonth, eDay, eHour, eMinute;
    cout << "set now at Expire Date? (Y/n) : ";
    std::cin >> Ent;
    Date EXD;
    if ('Y' == Ent)
    {
        time_t now = time(0);
        tm *Time = localtime (&now);
        eYear = Time->tm_year;
        eMonth = Time->tm_mon;
        eDay = Time->tm_mday;
        eHour = Time->tm_hour;
        eMinute = Time->tm_min;
        Date EXD(eYear, eMonth, eDay, eHour, eMinute, true);
    }
    else 
    {
        cout << "Please Enter Expire Year : ";
        std::cin >> eYear;
        while (eYear < pYear)
        {
            cerr << "!!!!!     Please Enter True Year     !!!!! : ";
            std::cin >> eYear;
        }
        cout << "Please Enter Expire Month : ";
        std::cin >> eMonth;
        while (eMonth < 1 || eMonth > 12 || (eYear == pYear && eMonth < pMonth))
        {
            cerr << "!!!!!     Please Enter True Month     !!!!! : ";
            std::cin >> eMonth;
        }
        cout << "Please Enter Expire Day : ";
        std::cin >> eDay;
        if (eMonth > 6)
        {
            while (eDay < 1 || eDay > 30 && (eYear == pYear && eMonth == pMonth && eDay < pDay))
            {
                cerr << "!!!!!     Please Enter True Day     !!!!! :";
                std::cin >> eDay;
            }    
        }
        else
        {
            while (eDay < 1 || eDay > 31 && (eYear == pYear && eMonth == pMonth && eDay < pDay))
            {
                cerr << "!!!!!     Please Enter True Day     !!!!! :";
                std::cin >> eDay;
            }    
        }
        
        
        cout << "Please Enter Expire Hour : ";
        std::cin >> eHour;
        while ((eHour < 0 || eHour > 23) && (eYear == pYear && eMonth == pMonth && eDay == pDay && eHour < pHour))
        {
            cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
            std::cin >> eHour;
        }
        cout << "Please Enter Expire Minute : ";
        std::cin >> eMinute;
        while (eMinute < 0 || eMinute > 59 && (eYear == pYear && eMonth == pMonth && eDay == pDay && eHour == pHour && eMinute < pMinute))
        {
            cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
            std::cin >> eMinute;
        }
        Date EXD(eYear, eMonth, eDay, eHour, eMinute, false);
    }
    good Kala(name, Id, KeepType, brand, PUD, EXD);
    store->addGood(Kala);
    
}
void deleteProduct (Store* store) {
    cout << "Enter good name that will be Remove : ";
    string dgName;
    std::cin >> dgName;
    bool isin = false;          //checking good available

    for (size_t i = 0; i < store->getGood().size(); i++)
    {
        if (store->getGood()[i].getCount() == 0)
        {
            cerr << "You can't delete an unavailable good!"<<endl;
        }
        else if (store->getGood()[i].getName() == dgName)
        {
            cerr << "Are you sure you want delete " << store->getGood()[i].getName()<<
            "  id: " << store->getGood()[i].getId() << " ? (Y or etc) : ";
            char sureCh;
            std::cin >> sureCh;
            if (sureCh == 'Y')
            {
                store->deleteGood(dgName);
            }
            else 
            {
                cerr << "Deleting canceled!" << endl;
                break;
            }
            
        }
        else
        {
            cerr << "No good found for delete !" << endl;
        }
        
        
    }
    
}
void editProduct (Store* store) {
    cout << "Please enter good name that you want change information : ";
    string lname;
    std::cin >> lname;
    for (size_t i = 0; i < store->getGood().size(); i++)
    {
        if (store->getGood()[i].getName() == lname)
        {
            cout << "Please Enter new name : ";
            string nName;
            std::cin >> nName;
            if (nName == lname)
            {
                store->getGood()[i].changeInfo(nName);
            }
            else
            {
                bool rptName = true;
                for (size_t j = 0; j < store->getGood().size(); j++)
                {
                    if (store->getGood()[j].getName() == nName)
                    {
                        cerr<<"This Name existing !"<<endl;
                        break;
                    }  
                } 
                store->getGood()[i].changeInfo(nName);
            }
        }
        
    }
    
}
void showProduct (Store store) {
    cout << "Enter good name to show : ";
    string shName;
    std::cin >> shName;
    bool isin = true;
    for (size_t i = 0; i < store.getGood().size(); i++)
    {
        if(store.getGood()[i].getName() == shName)
        {
            cout << right << setw(6) << "ID" << right << setw(15) << "GoodName"<<
            right << setw(15) << "KeepingType" << right << setw(15) << "Brand" <<
            right << setw(17) << "Pub" << right << setw(17) << "Exp" << endl;
            cout << right << setw(6) <<store.getGood()[i].getId()<< right << setw(15) <<store.getGood()[i].getName()<<
            right << setw(15) <<store.getGood()[i].getKeepType()<< right << setw(15) <<store.getGood()[i].getBrand()<<
            right << setw(17) << store.getGood()[i].getPubDate().getFDate()<< right << setw(17) <<store.getGood()[i].getPubDate().getFDate()<< endl; 
        }
    }
}
void findProduct (Store store) {
    cout << "Enter your word : ";
    string srch;
    std::cin >> srch;
    int wordSize = srch.length();
    bool isFind = true;
    bool isFirst = true;
    for (size_t i = 0; i < store.getGood().size(); i++)
    {
        for (size_t j = 0; j < store.getGood()[i].getName().length() - wordSize; j++)
        {
            isFind = true;
            for (size_t k = 0; k < wordSize; k++)
            {
                if (store.getGood()[i].getName()[k] != srch[k])
                {
                    isFind = false;
                    break;
                }
                
            }
            if (isFind && isFirst)
            {
                cout << right << setw(6) << "ID" << right << setw(15) << "GoodName"<<
                right << setw(15) << "KeepingType" << right << setw(15) << "Brand" <<
                right << setw(17) << "Pub" << right << setw(17) << "Exp" << endl;
                isFirst = false;
            }
            if (isFind)
            {
                cout << right << setw(6) <<store.getGood()[i].getId()<< right << setw(15) <<store.getGood()[i].getName()<<
                right << setw(15) <<store.getGood()[i].getKeepType()<< right << setw(15) <<store.getGood()[i].getBrand()<<
                right << setw(17) << store.getGood()[i].getPubDate().getFDate()<< right << setw(17) <<store.getGood()[i].getPubDate().getFDate()<< endl; 
                break;
            }
            
            
        }
        
    }
    
}
void buyProduct (Store* store) {
    int Id;
    bool rptId = true;
    while (rptId)
    {
        srand(time(NULL));
        try
        {
            Id = rand()%900 + 99;
            for (size_t i = 0; i < store->getByIds().size(); i++)
            {
                if (store->getByIds()[i] == Id)
                {
                    throw std::invalid_argument("Repeated Id");
                }
            }
        }
        catch(std::invalid_argument)
        {
            continue;
        }
        rptId = false;
    }
    
    cout << "Enter your pruduct id : ";
    int prId;
    std::cin >> prId;
    bool isIn = false;
    size_t i;
    for (i = 0; i < store->getGood().size(); i++)
    {
        if (store->getGood()[i].getId() == prId)
        {
            isIn = true;
            break;
        }   
    }
    if(!isIn)
    {
        cout << "This Product doesn't find!";
        return;
    }
    cout << "Enter product number : ";
    int prNum;
    std::cin >> prNum;
    while (prNum < 0)
    {
        cerr << "You most buy at least one of this product, reEnter number : ";
        std::cin >> prNum;
    }
    store->getGood()[i].addCount(prNum);
    cout << "Enter product Price : ";
    int prPrc;
    std::cin >> prPrc;
    while (prPrc < 0)
    {
        cerr << "Please Enter true value : ";
        std::cin >> prPrc;
    }
    int bYear, bMonth, bDay, bHour, bMinute;
    std::cout << "set now at Buy Date? (Y/n) : ";
    char Ent;
    std::cin >> Ent;
    Date BD;
    if ('Y' == Ent)
    {
        time_t now = time(0);
        tm *Time = localtime (&now);
        bYear = Time->tm_year;
        bMonth = Time->tm_mon;
        bDay = Time->tm_mday;
        bHour = Time->tm_hour;
        bMinute = Time->tm_min;
        Date BD(bYear, bMonth, bDay, bHour, bMinute, true);
    }
    else 
    {
        std::cout << "Please Enter Buy Year : ";
        std::cin >> bYear;
        while (bYear < 1300)
        {
        std::cerr << "!!!!!     Please Enter True Year     !!!!! : ";
        std::cin >> bYear;
        }
        std::cout << "Please Enter Buy Month : ";
        std::cin >> bMonth;
        while (bMonth < 1 || bMonth > 12)
        {
        std::cerr << "!!!!!     Please Enter True Month     !!!!! : ";
        std::cin >> bMonth;
        }
        std::cout << "Please Enter Buy Day : ";
        std::cin >> bDay;
        if (bMonth > 6)
        {
        while (bDay < 1 || bDay > 30)
        {
            std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
            std::cin >> bDay;
        }    
        }
        else
        {
        while (bDay < 1 || bDay > 31)
        {
            std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
            std::cin >> bDay;
        }    
        }
        std::cout << "Please Enter Buy Hour : ";
        std::cin >> bHour;
        while (bHour < 0 || bHour > 23)
        {
        std::cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
        std::cin >> bHour;
        }
        std::cout << "Please Enter Buy Minute : ";
        std::cin >> bMinute;
        while (bMinute < 0 || bMinute > 59)
        {
        std::cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
        std::cin >> bMinute;
        }
        Date BD(bYear, bMonth, bDay, bHour, bMinute, false);
    }
    Buy bought (prId, prPrc, prNum, BD);
    store->addBoughts(bought);
    
}
void sellProduct (Store* store) {
    cout << "Enter your pruduct id : ";
    int prId;
    std::cin >> prId;
    bool isIn = false;
    size_t i;
    for (i = 0; i < store->getGood().size(); i++)
    {
        if (store->getGood()[i].getId() == prId)
        {
            isIn = true;
            break;
        }   
    }
    if(!isIn)
    {
        cout << "This Product doesn't find!";
        return;
    }
    cout << "Enter product number : ";
    int prNum;
    std::cin >> prNum;
    while (prNum < 0)
    {
        cerr << "You most buy at least one of this product, reEnter number : ";
        std::cin >> prNum;
    }
    store->getGood()[i].minCount(prNum);
    cout << "Enter product Price : ";
    int prPrc;
    std::cin >> prPrc;
    while (prPrc < 0)
    {
        cerr << "Please Enter true value : ";
        std::cin >> prPrc;
    }
    cout << "Enter product reduction : ";
    int prOff;
    std::cin >> prOff;
    while (prOff < 0)
    {
        cerr << "Please Enter true value : ";
        std::cin >> prOff;
    }
    int sYear, sMonth, sDay, sHour, sMinute;
    std::cout << "set now at Sell Date? (Y/n) : ";
    char Ent;
    std::cin >> Ent;
    Date SD;
    if ('Y' == Ent)
    {
        time_t now = time(0);
        tm *Time = localtime (&now);
        sYear = Time->tm_year;
        sMonth = Time->tm_mon;
        sDay = Time->tm_mday;
        sHour = Time->tm_hour;
        sMinute = Time->tm_min;
        Date SD(sYear, sMonth, sDay, sHour, sMinute, true);
    }
    else 
    {
        std::cout << "Please Enter Sell Year : ";
        std::cin >> sYear;
        while (sYear < 1300)
        {
        std::cerr << "!!!!!     Please Enter True Year     !!!!! : ";
        std::cin >> sYear;
        }
        std::cout << "Please Enter Sell Month : ";
        std::cin >> sMonth;
        while (sMonth < 1 || sMonth > 12)
        {
        std::cerr << "!!!!!     Please Enter True Month     !!!!! : ";
        std::cin >> sMonth;
        }
        std::cout << "Please Enter Sell Day : ";
        std::cin >> sDay;
        if (sMonth > 6)
        {
        while (sDay < 1 || sDay > 30)
        {
            std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
            std::cin >> sDay;
        }    
        }
        else
        {
        while (sDay < 1 || sDay > 31)
        {
            std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
            std::cin >> sDay;
        }    
        }
        std::cout << "Please Enter Sell Hour : ";
        std::cin >> sHour;
        while (sHour < 0 || sHour > 23)
        {
        std::cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
        std::cin >> sHour;
        }
        std::cout << "Please Enter Sell Minute : ";
        std::cin >> sMinute;
        while (sMinute < 0 || sMinute > 59)
        {
        std::cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
        std::cin >> sMinute;
        }
        Date BD(sYear, sMonth, sDay, sHour, sMinute, false);
    }
    Sold bought(prId, prPrc, prNum, SD, prOff);
    store->addSolds(bought);

}
void bill (Store store) {
    cout << left << setw(4) << "1."  << right << setw(25) << "One Customer and a date range" << setw(8) << "Sell" <<endl;
    cout << left << setw(4) << "2."  << right << setw(25) << "Just one product name" << setw(8) << "Sell" <<endl;
    cout << left << setw(4) << "3."  << right << setw(25) << "Just one product name" << setw(8) << "Buy" <<endl;
    cout << left << setw(4) << "4."  << right << setw(25) << "Just a date range" << setw(8) << "Buy" <<endl;
    cout << "Please Enter Type of Bill number : ";
    int type;
    cin >> type;
    switch (type)
    {
        case 1:
            {
                ofstream bill;
                bill.open("Bill.txt", ios::out);

                int sYear, sMonth, sDay, sHour, sMinute;
                    std::cout << "Please Enter first date Year : ";
                    std::cin >> sYear;
                    while (sYear < 1300)
                    {
                    std::cerr << "!!!!!     Please Enter True Year     !!!!! : ";
                    std::cin >> sYear;
                    }
                    std::cout << "Please Enter first date Month : ";
                    std::cin >> sMonth;
                    while (sMonth < 1 || sMonth > 12)
                    {
                        std::cerr << "!!!!!     Please Enter True Month     !!!!! : ";
                        std::cin >> sMonth;
                    }
                    std::cout << "Please Enter first date Day : ";
                    std::cin >> sDay;
                    if (sMonth > 6)
                    {
                        while (sDay < 1 || sDay > 30)
                        {
                            std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                            std::cin >> sDay;
                        }    
                    }
                    else
                    {
                    while (sDay < 1 || sDay > 31)
                    {
                        std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                        std::cin >> sDay;
                    }    
                    }
                    std::cout << "Please Enter first date Hour : ";
                    std::cin >> sHour;
                    while (sHour < 0 || sHour > 23)
                    {
                        std::cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
                        std::cin >> sHour;
                    }
                    std::cout << "Please Enter first date Minute : ";
                    std::cin >> sMinute;
                    while (sMinute < 0 || sMinute > 59)
                    {
                        std::cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
                        std::cin >> sMinute;
                    }
                    Date bill_1(sYear, sMonth, sDay, sHour, sMinute, false);
                    
                    
                    int pYear, pMonth, pDay, pHour, pMinute;
                    cout << "set now at Second Date? (Y/n) : ";
                    char Ent;
                    std::cin >> Ent;
                    Date bill_2;
                    if ('Y' == Ent)
                    {
                        time_t now = time(0);
                        tm *Time = localtime (&now);
                        pYear = Time->tm_year;
                        pMonth = Time->tm_mon;
                        pDay = Time->tm_mday;
                        pHour = Time->tm_hour;
                        pMinute = Time->tm_min;
                        Date bill_2(pYear, pMonth, pDay, pHour, pMinute, true);
                    }
                    else 
                    {
                        cout << "Please Enter Second date Year : ";
                        std::cin >> pYear;
                        while (pYear < 1300)
                        {
                            cerr << "!!!!!     Please Enter True Year     !!!!! : ";
                            std::cin >> pYear;
                        }
                        cout << "Please Enter Second date Month : ";
                        std::cin >> pMonth;
                        while (pMonth < 1 || pMonth > 12)
                        {
                            cerr << "!!!!!     Please Enter True Month     !!!!! : ";
                            std::cin >> pMonth;
                        }
                        cout << "Please Enter Second date Day : ";
                        std::cin >> pDay;
                        if (pMonth > 6)
                        {
                            while (pDay < 1 || pDay > 30)
                            {
                                cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                                std::cin >> pDay;
                            }    
                        }
                        else
                        {
                            while (pDay < 1 || pDay > 31)
                            {
                                cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                                std::cin >> pDay;
                            }    
                        }
                        cout << "Please Enter Second date Hour : ";
                        std::cin >> pHour;
                        while (pHour < 0 || pHour > 23)
                        {
                            cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
                            std::cin >> pHour;
                        }
                        cout << "Please Enter Second date Minute : ";
                        std::cin >> pMinute;
                        while (pMinute < 0 || pMinute > 59)
                        {
                            cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
                            std::cin >> pMinute;
                        }
                        Date bill_2(pYear, pMonth, pDay, pHour, pMinute, false);
                    }
                
                cout << "Enter your Customer id : ";
                int csId;
                std::cin >> csId;
                bool isIn = false;
                size_t i;
                for (i = 0; i < store.getSolds().size(); i++)
                {
                    if (store.getSolds()[i].getCusId() == csId)
                    {
                        isIn = true;
                        break;
                    }
                }
                if(!isIn)
                {
                    cout << "No Sold with this id!";
                    return;
                }
                if(isIn)
                {
                    cout << right << setw(12) << "Customer Id" << setw(8) << "Good Id" << setw(5) << "No" << setw(12) << "Price" <<
                    setw(18) << "Date" << setw(12) << "Reduction" << endl;
                    bill << right << setw(12) << "Customer Id" << setw(8) << "Good Id" << setw(5) << "No" << setw(12) << "Price" <<
                    setw(18) << "Date" << setw(12) << "Reduction" << endl;
                }
                else 
                {
                    for (size_t i = 0; i < store.getSolds().size(); i++)
                    {
                        //checking range
                        bool isOk = true;
                        Date dateOk;
                        dateOk = store.getSolds()[i].getDate();
                        if (dateOk.getYear() < bill_1.getYear() || dateOk.getYear() > bill_2.getYear())
                        {
                            isOk = false;
                        }
                        else if (dateOk.getMonth() < bill_1.getMonth() || dateOk.getMonth() > bill_2.getMonth())
                        {
                            isOk = false;
                        }
                        else if (dateOk.getDay() < bill_1.getDay() || dateOk.getDay() > bill_2.getDay())
                        {
                            isOk = false;
                        }
                        else if (dateOk.getHour() < bill_1.getHour() || dateOk.getHour() > bill_2.getHour())
                        {
                            isOk = false;
                        }
                        else if (dateOk.getMinute() < bill_1.getMinute() || dateOk.getMinute() > bill_2.getMinute())
                        {
                            isOk = false;
                        }
                        //check customerID
                        if (store.getSolds()[i].getCusId() != csId)
                        {
                            isOk = false;
                        }
                        
                        if (isOk)
                        {
                            cout << right << setw(12) << store.getSolds()[i].getCusId() << setw(8) << store.getSolds()[i].getGoodId() << setw(5) <<
                            store.getSolds()[i].getCount() << setw(12) << store.getSolds()[i].Getprice() << setw(18) << store.getSolds()[i].getDate().getFDate() <<
                            setw(12) << store.getSolds()[i].getOff() << endl;
                            bill << right << setw(12) << store.getSolds()[i].getCusId() << setw(8) << store.getSolds()[i].getGoodId() << setw(5) <<
                            store.getSolds()[i].getCount() << setw(12) << store.getSolds()[i].Getprice() << setw(18) << store.getSolds()[i].getDate().getFDate() <<
                            setw(12) << store.getSolds()[i].getOff() << endl;
                        }
                        
                    }
                    
                }

                break;
            }
        case 2:
            {
                cout << "Enter good Name : ";
                string gdName;
                cin >> gdName;
                bool isin = false;
                int gdId;
                for (size_t i = 0; i < store.getGood().size(); i++)
                {
                    if (store.getGood()[i].getName() == gdName)
                    {
                        isin = true;
                        gdId = store.getGood()[i].getId();
                        cout << right << setw(8) << "Good Id" << setw(5) << "No" << setw(12) << "Price" << setw(12) << "Reduction" << endl;
                    }
                }
                if (!isin)
                {
                    cerr << "Good doesn't found!"<<endl;
                    return;
                }
                else
                {
                    for (size_t i = 0; i < store.getSolds().size(); i++)
                    {
                        if (store.getSolds()[i].getGoodId() == gdId)
                        {
                            cout << right << setw(8) << store.getSolds()[i].getGoodId() << setw(5) << store.getSolds()[i].getCount()
                            << setw(12) << store.getSolds()[i].Getprice() << setw(12) << store.getSolds()[i].getOff() << endl;
                        }
                        
                    }
                    
                }
                break;
            }
        case 3:
            {
                cout << "Enter good Name : ";
                string gdName;
                cin >> gdName;
                bool isin = false;
                int gdId;
                for (size_t i = 0; i < store.getGood().size(); i++)
                {
                    if (store.getGood()[i].getName() == gdName)
                    {
                        isin = true;
                        gdId = store.getGood()[i].getId();
                        cout << right << setw(8) << "Good Id" << setw(5) << "No" << setw(12) << "Price" << setw(15) << "Date" << endl;
                    }
                }
                if (!isin)
                {
                    cerr << "Good doesn't found!"<<endl;
                    return;
                }
                else
                {
                    int priceSum = 0;
                    int numSum = 0;
                    for (size_t i = 0; i < store.getBoughts().size(); i++)
                    {
                        if (store.getBoughts()[i].getGoodId() == gdId)
                        {
                            cout << right << setw(8) << store.getSolds()[i].getGoodId() << setw(5) << store.getSolds()[i].getCount()
                            << setw(12) << store.getSolds()[i].Getprice() << setw(12) << store.getSolds()[i].getDate().getFDate() << endl;
                            priceSum +=  (store.getSolds()[i].getCount()) * (store.getSolds()[i].Getprice());
                            numSum += store.getSolds()[i].getCount();
                        }
                        
                    }
                    cout << endl;
                    cout << right << setw(8) << "Sum :" << setw(5) << numSum << setw(12) << priceSum << setw(12) << endl;
                }
                

                break;
            }
        case 4:
            {
                int sYear, sMonth, sDay, sHour, sMinute;
                    std::cout << "Please Enter first date Year : ";
                    std::cin >> sYear;
                    while (sYear < 1300)
                    {
                    std::cerr << "!!!!!     Please Enter True Year     !!!!! : ";
                    std::cin >> sYear;
                    }
                    std::cout << "Please Enter first date Month : ";
                    std::cin >> sMonth;
                    while (sMonth < 1 || sMonth > 12)
                    {
                        std::cerr << "!!!!!     Please Enter True Month     !!!!! : ";
                        std::cin >> sMonth;
                    }
                    std::cout << "Please Enter first date Day : ";
                    std::cin >> sDay;
                    if (sMonth > 6)
                    {
                        while (sDay < 1 || sDay > 30)
                        {
                            std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                            std::cin >> sDay;
                        }    
                    }
                    else
                    {
                    while (sDay < 1 || sDay > 31)
                    {
                        std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                        std::cin >> sDay;
                    }    
                    }
                    std::cout << "Please Enter first date Hour : ";
                    std::cin >> sHour;
                    while (sHour < 0 || sHour > 23)
                    {
                        std::cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
                        std::cin >> sHour;
                    }
                    std::cout << "Please Enter first date Minute : ";
                    std::cin >> sMinute;
                    while (sMinute < 0 || sMinute > 59)
                    {
                        std::cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
                        std::cin >> sMinute;
                    }
                    Date bill_1(sYear, sMonth, sDay, sHour, sMinute, false);
                    
                    
                    int pYear, pMonth, pDay, pHour, pMinute;
                    cout << "set now at Second Date? (Y/n) : ";
                    char Ent;
                    std::cin >> Ent;
                    Date bill_2;
                    if ('Y' == Ent)
                    {
                        time_t now = time(0);
                        tm *Time = localtime (&now);
                        pYear = Time->tm_year;
                        pMonth = Time->tm_mon;
                        pDay = Time->tm_mday;
                        pHour = Time->tm_hour;
                        pMinute = Time->tm_min;
                        Date bill_2(pYear, pMonth, pDay, pHour, pMinute, true);
                    }
                    else 
                    {
                        cout << "Please Enter Second date Year : ";
                        std::cin >> pYear;
                        while (pYear < 1300)
                        {
                            cerr << "!!!!!     Please Enter True Year     !!!!! : ";
                            std::cin >> pYear;
                        }
                        cout << "Please Enter Second date Month : ";
                        std::cin >> pMonth;
                        while (pMonth < 1 || pMonth > 12)
                        {
                            cerr << "!!!!!     Please Enter True Month     !!!!! : ";
                            std::cin >> pMonth;
                        }
                        cout << "Please Enter Second date Day : ";
                        std::cin >> pDay;
                        if (pMonth > 6)
                        {
                            while (pDay < 1 || pDay > 30)
                            {
                                cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                                std::cin >> pDay;
                            }    
                        }
                        else
                        {
                            while (pDay < 1 || pDay > 31)
                            {
                                cerr << "!!!!!     Please Enter True Day     !!!!! : ";
                                std::cin >> pDay;
                            }    
                        }
                        cout << "Please Enter Second date Hour : ";
                        std::cin >> pHour;
                        while (pHour < 0 || pHour > 23)
                        {
                            cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
                            std::cin >> pHour;
                        }
                        cout << "Please Enter Second date Minute : ";
                        std::cin >> pMinute;
                        while (pMinute < 0 || pMinute > 59)
                        {
                            cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
                            std::cin >> pMinute;
                        }
                        Date bill_2(pYear, pMonth, pDay, pHour, pMinute, false);
                }
                if (store.getBoughts().size() != 0)
                {
                    cout << right << setw(5) << "No" << setw(12) << "Price" << setw(18) << "Date" << endl;
                }
                for (size_t i = 0; i < store.getBoughts().size(); i++)
                {
                    //checking range
                    bool isOk = true;
                    Date dateOk;
                    dateOk = store.getSolds()[i].getDate();
                    if (dateOk.getYear() < bill_1.getYear() || dateOk.getYear() > bill_2.getYear())
                    {
                        isOk = false;
                    }
                    else if (dateOk.getMonth() < bill_1.getMonth() || dateOk.getMonth() > bill_2.getMonth())
                    {
                        isOk = false;
                    }
                    else if (dateOk.getDay() < bill_1.getDay() || dateOk.getDay() > bill_2.getDay())
                    {
                        isOk = false;
                    }
                    else if (dateOk.getHour() < bill_1.getHour() || dateOk.getHour() > bill_2.getHour())
                    {
                        isOk = false;
                    }
                    else if (dateOk.getMinute() < bill_1.getMinute() || dateOk.getMinute() > bill_2.getMinute())
                    {
                        isOk = false;
                    }
                    
                    if (isOk)
                    {
                        cout << right << setw(5) << store.getBoughts()[i].getCount() << setw(12) << store.getBoughts()[i].Getprice()
                        << setw(18) << store.getBoughts()[i].getDate().getFDate() << endl;
                    }
                }
                
                
                break;
            }

        default:
            cerr << "Your input wasn't good !"<<endl;
            break;
    }



}



