#include "MyHeaders/Date.hpp"

long* GTJ(long, long, long, long[]);


Date::Date(unsigned int Y,unsigned int MO,unsigned int D,unsigned int H,unsigned int M, bool ByComp):
Hour(H),Minute(M) {
  if (ByComp)
  {
    long ymd[3];
    GTJ(Y,MO,D,ymd);
    this->Year = ymd[0];
    this->Month = ymd[1];
    this->Day = ymd[2];
  }
  else
  {
    this->Year = Y;
    this->Month = MO;
    this->Day = D;
  }
}

Date::Date(){}

std::string Date::getTime() {
  std::string hour;
  if (this->Hour < 10)
  {
    hour = "0" + std::to_string(this->Hour);
  }
  else
  {
    hour = std::to_string(this->Hour);
  }
  std::string minute;
  if (this->Minute < 10)
  {
    minute = "0" + std::to_string(this->Minute);
  }
  else
  {
    minute = std::to_string(this->Minute);
  }
  return hour + ":" + minute;
}

std::string Date::getDate() {
  std::string month;
  if (this->Month < 10)
  {
    month = "0" + std::to_string(this->Month);
  }
  else
  {
    month = std::to_string(this->Month);
  }
  std::string day;
  if (this->Day < 10)
  {
    day = "0" + std::to_string(this->Day);
  }
  else
  {
    day = std::to_string(this->Day);
  }  
  return std::to_string(this->Year)+ "/" +month+ "/" +day;
}

std::string Date::getFDate() {
  return this->getDate() + " " + this->getTime();
}

unsigned int Date::getYear() {
  return this->Year;
}

unsigned int Date::getMonth() {
  return this->Month;
}

unsigned int Date::getDay() {
  return this->Day;
}

unsigned int Date::getHour() {
  return this->Hour;
}

unsigned int Date::getMinute() {
  return this->Minute;
}


long* GTJ(long gy,long gm,long gd,long out[]){
  long jy;
  long g_d_m[12]={0,31,59,90,120,151,181,212,243,273,304,334};
  if(gy>1600)
  {
    jy=979;
    gy-=1600;
  }
  else
  {
    jy=0;
    gy-=621;
  }
  long gy2=(gm>2)?(gy+1):gy;
  long days=(365*gy) +((int)((gy2+3)/4)) -((int)((gy2+99)/100)) +((int)((gy2+399)/400)) -80 +gd +g_d_m[gm-1];
  jy+=33*((int)(days/12053));
  days%=12053;
  jy+=4*((int)(days/1461));
  days%=1461;
  if(days > 365)
  {
    jy+=(int)((days-1)/365);
    days=(days-1)%365;
  }
  long jm=(days < 186)?1+(int)(days/31):7+(int)((days-186)/30);
  long jd=1+((days < 186)?(days%31):((days-186)%30));
  out[0]=jy;
  out[1]=jm;
  out[2]=jd;
  return out;
}
