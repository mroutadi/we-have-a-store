#include "MyHeaders/Store.hpp"

Store::Store(){

}

void Store::addSolds(Sold sold) {
  this->Solds.push_back(sold);
}

std::vector<Sold> Store::getSolds() {
  return this->Solds;
}

std::vector <int> Store::getByIds() {
  return this->byIds;
}

std::vector <int> Store::getGdIds() {
  return this->gdIds;
}

std::vector <int> Store::getSlIds() {
  return this->slIds;
}

void Store::addBoughts(Buy buy) {
  this->Boughts.push_back(buy);
  this->byIds.push_back(buy.getGoodId());
}

std::vector<Buy> Store::getBoughts() {
  return this->Boughts;
}

void Store::addGood(good good) {
  this->Goods.push_back(good);
  this->gdIds.push_back(good.getId());
}

std::vector<good> Store::getGood() {
  return this->Goods;
}

void Store::deleteGood(std::string name)
{
  std::vector <good> newGdVec;
  int deletedId = 0;
  for (size_t j = 0; j < this->getGood().size(); j++)
  {
    if (this->getGood()[j].getName() != name)
    {
      newGdVec.push_back(this->getGood()[j]);
    }
    else
    {
      deletedId = this->getGood()[j].getId();
      continue;
    }
    this->Goods = newGdVec; 
  }
  for (size_t j = 0; j < this->getBoughts().size(); j++)
  {
    if (this->getBoughts()[j].getGoodId() != deletedId)
    {
      newGdVec.push_back(this->getGood()[j]);
    }
    else
    {
      continue;
    }
    this->Goods = newGdVec; 
  }
}
