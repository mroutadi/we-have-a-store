#include "MyHeaders/good.hpp"

good::good(std::string N, int ID, std::string KT, std::string B, Date P, Date E):name(N),
KeepType(KT), Brand(B), publish(P), expiration(E),goodCount(0),id(ID) {

}

void good::changeInfo(std::string newName) {
  this->name = newName;
  int KeepTypenum;
  std::string KeepType;
  std::cout << std::left << std::setw(4) << "1." << std::setw(8) << "NDist" <<std::endl;
  std::cout << std::left << std::setw(4) << "2." << std::setw(8) << "DNRef" <<std::endl;
  std::cout << std::left << std::setw(4) << "3." << std::setw(8) << "NRef " <<std::endl;
  std::cout << "Enter Keeping Type Number : ";
  std::cin >> KeepTypenum;
  while (KeepTypenum > 3 || KeepTypenum < 1)
  {
    std::cerr << "!!!!!     Please Enter True Option     !!!!! : ";
    std::cin >> KeepTypenum;
  }
  switch (KeepTypenum)
  {
    case 1:
    KeepType = "NDist";
    break;

    case 2:
    KeepType = "DNRef";
    break;

    case 3:
    KeepType = "NRef";
    break;
  }
  std::string brand;
  std::cout << "Please Enter Good Brand : ";
  std::cin >> brand;
  while (brand == "")
  {
    std::cerr << "!!!!!     Please Enter At Least 1 Character for Brand     !!!!! : ";
    std::cin >> brand;
  }
  int pYear, pMonth, pDay, pHour, pMinute;
  std::cout << "set now at Publish Date? (Y/n) : ";
  char Ent;
  std::cin >> Ent;
  Date PUD;
  if ('Y' == Ent)
  {
    time_t now = time(0);
    tm *Time = localtime (&now);
    pYear = Time->tm_year;
    pMonth = Time->tm_mon;
    pDay = Time->tm_mday;
    pHour = Time->tm_hour;
    pMinute = Time->tm_min;
    Date PUD(pYear, pMonth, pDay, pHour, pMinute, true);
  }
  else 
  {
    std::cout << "Please Enter Publish Year : ";
    std::cin >> pYear;
    while (pYear < 1300)
    {
      std::cerr << "!!!!!     Please Enter True Year     !!!!! : ";
      std::cin >> pYear;
    }
    std::cout << "Please Enter Publish Month : ";
    std::cin >> pMonth;
    while (pMonth < 1 || pMonth > 12)
    {
      std::cerr << "!!!!!     Please Enter True Month     !!!!! : ";
      std::cin >> pMonth;
    }
    std::cout << "Please Enter Publish Day : ";
    std::cin >> pDay;
    if (pMonth > 6)
    {
      while (pDay < 1 || pDay > 30)
      {
        std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
        std::cin >> pDay;
      }    
    }
    else
    {
      while (pDay < 1 || pDay > 31)
      {
        std::cerr << "!!!!!     Please Enter True Day     !!!!! : ";
        std::cin >> pDay;
      }    
    }
    std::cout << "Please Enter Publish Hour : ";
    std::cin >> pHour;
    while (pHour < 0 || pHour > 23)
    {
      std::cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
      std::cin >> pHour;
    }
    std::cout << "Please Enter Publish Minute : ";
    std::cin >> pMinute;
    while (pMinute < 0 || pMinute > 59)
    {
      std::cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
      std::cin >> pMinute;
    }
    Date PUD(pYear, pMonth, pDay, pHour, pMinute, false);
  }
  int eYear, eMonth, eDay, eHour, eMinute;
  std::cout << "set now at Expire Date? (Y/n) : ";
  std::cin >> Ent;
  Date EXD;
  if ('Y' == Ent)
  {
    time_t now = time(0);
    tm *Time = localtime (&now);
    eYear = Time->tm_year;
    eMonth = Time->tm_mon;
    eDay = Time->tm_mday;
    eHour = Time->tm_hour;
    eMinute = Time->tm_min;
    Date EXD(eYear, eMonth, eDay, eHour, eMinute, true);
  }
  else 
  {
    std::cout << "Please Enter Expire Year : ";
    std::cin >> eYear;
    while (eYear < pYear)
    {
      std::cerr << "!!!!!     Please Enter True Year     !!!!! : ";
      std::cin >> eYear;
    }
    std::cout << "Please Enter Expire Month : ";
    std::cin >> eMonth;
    while (eMonth < 1 || eMonth > 12 || (eYear == pYear && eMonth < pMonth))
    {
      std::cerr << "!!!!!     Please Enter True Month     !!!!! : ";
      std::cin >> eMonth;
    }
    std::cout << "Please Enter Expire Day : ";
    std::cin >> eDay;
    if (eMonth > 6)
    {
      while (eDay < 1 || eDay > 30 && (eYear == pYear && eMonth == pMonth && eDay < pDay))
      {
        std::cerr << "!!!!!     Please Enter True Day     !!!!! :";
        std::cin >> eDay;
      }    
    }
    else
    {
      while (eDay < 1 || eDay > 31 && (eYear == pYear && eMonth == pMonth && eDay < pDay))
      {
        std::cerr << "!!!!!     Please Enter True Day     !!!!! :";
        std::cin >> eDay;
      }    
    }
    
    
    std::cout << "Please Enter Expire Hour : ";
    std::cin >> eHour;
    while ((eHour < 0 || eHour > 23) && (eYear == pYear && eMonth == pMonth && eDay == pDay && eHour < pHour))
    {
      std::cerr << "!!!!!     Please Enter True Hour (0 < Hour < 24)     !!!!! : ";
      std::cin >> eHour;
    }
    std::cout << "Please Enter Expire Minute : ";
    std::cin >> eMinute;
    while (eMinute < 0 || eMinute > 59 && (eYear == pYear && eMonth == pMonth && eDay == pDay && eHour == pHour && eMinute < pMinute))
    {
      std::cerr << "!!!!!     Please Enter True Minute     !!!!! : ";
      std::cin >> eMinute;
    }
    Date EXD(eYear, eMonth, eDay, eHour, eMinute, false);
  }
  this->Brand = brand;
  this->expiration = EXD;
  this->KeepType = KeepType;
  this->name = name;
  this->publish = PUD;
} 
 
std::string good::getName() {
  return this->name;
}

int good::getId() {
  return this->id;
}

std::string good::getKeepType() {
  return this->KeepType;
}

std::string good::getBrand() {
  return this->Brand;
}

int good::getCount() {
  return this->goodCount;
}

Date good::getPubDate() {
  return this->publish;
}

Date good::getExpDate() {
  return this->expiration;
}

void good::addCount(int count) {
  this->goodCount += count;
}

void good::minCount(int count) {
  this->goodCount -= count;
}
/*
good good::operator()(Date bill_1, Date bill_2, bool checker) {
  if(!checker) return;
  std::cout << std::right << std::setw(5) << "No" << std::setw(12) << "Price" << std::setw(18) << "Date" << std::endl;
  for (size_t i = 0; i < goodStore.getBoughts().size(); i++)
  {
    //checking range
    bool isOk = true;
    Date dateOk;
    dateOk = goodStore.getSolds()[i].getDate();
    if (dateOk.getYear() < bill_1.getYear() || dateOk.getYear() > bill_2.getYear())
    {
      isOk = false;
    }
    else if (dateOk.getMonth() < bill_1.getMonth() || dateOk.getMonth() > bill_2.getMonth())
    {
      isOk = false;
    }
    else if (dateOk.getDay() < bill_1.getDay() || dateOk.getDay() > bill_2.getDay())
    {
      isOk = false;
    }
    else if (dateOk.getHour() < bill_1.getHour() || dateOk.getHour() > bill_2.getHour())
    {
      isOk = false;
    }
    else if (dateOk.getMinute() < bill_1.getMinute() || dateOk.getMinute() > bill_2.getMinute())
    {
      isOk = false;
    }
    
    if (isOk)
    {
      std::cout << std::right << std::setw(5) << goodStore.getBoughts()[i].getCount() << std::setw(12) << goodStore.getBoughts()[i].Getprice()
      << std::setw(18) << goodStore.getBoughts()[i].getDate().getFDate() << std::endl;
    }
  }
                
}
*/